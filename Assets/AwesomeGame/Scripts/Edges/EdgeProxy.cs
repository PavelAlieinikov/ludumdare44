using System;
using AwesomeGame.Edges.Components;
using Unity.Entities;

namespace AwesomeGame.Edges
{
    public class EdgeProxy: ComponentDataProxy<Edge>
    {
        protected override void ValidateSerializedData(ref Edge serializedData)
        {
            serializedData.Id = Guid.NewGuid();
        }
    }
}