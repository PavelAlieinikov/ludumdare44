using System;
using AwesomeGame.Regions;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Edges
{
    public class EdgeLinks : MonoBehaviour
    {
        [SerializeField]
        private GameObjectEntity _regionA;
        [SerializeField]
        private GameObjectEntity _regionB;

        public Entity RegionA => _regionA.Entity;
        public Entity RegionB => _regionB.Entity;

    }
}