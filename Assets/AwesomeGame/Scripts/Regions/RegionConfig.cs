using UnityEngine;

namespace AwesomeGame.Regions
{
    public class RegionConfig : MonoBehaviour
    {
        [SerializeField]
        private int _startHeretics;
        [SerializeField]
        private int _startBelievers;
        [SerializeField]
        private float _startFaith;

        public int StartHeretics => _startHeretics;
        public int StartBelievers => _startBelievers;
        public float StartFaith => _startFaith;
        public bool AIActiveOnStart;
    }
}