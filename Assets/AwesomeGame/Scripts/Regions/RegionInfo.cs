using UnityEngine;

namespace AwesomeGame.Regions
{
    public class RegionInfo : MonoBehaviour
    {
        public string Name;

        public Sprite Icon;
    }
}