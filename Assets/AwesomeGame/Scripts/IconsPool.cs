using System;
using System.Collections.Generic;
using UnityEngine;

namespace AwesomeGame
{
    public enum IconType
    {
        Army,
        Victory,
        Defeat,
        Migration,
        Migrants,
    }

    public class IconsPool : MonoBehaviour
    {
        public GameObject ArmyPrefab;
        public GameObject MigrantsPrefab;
        public GameObject VictoryPrefab;
        public GameObject DefeatPrefab;
        public GameObject MigrationPrefab;

        public Transform Create(IconType type)
        {
            var position = Vector3.one * 100000;
            GameObject prefab = null;
            switch (type)
            {
                case IconType.Army:
                    prefab = ArmyPrefab;
                    break;
                case IconType.Victory:
                    prefab = VictoryPrefab;
                    break;
                case IconType.Defeat:
                    prefab = DefeatPrefab;
                    break;
                case IconType.Migration:
                    prefab = MigrationPrefab;
                    break;
                case IconType.Migrants:
                    prefab = MigrantsPrefab;
                    break;
            }
            var instance = Instantiate(prefab, position, Quaternion.identity);
            instance.transform.SetParent(transform);
            return instance.transform;
        }

        public void Cleanup()
        {
            var queue = new Queue<GameObject>();
            foreach (Transform child in transform)
                queue.Enqueue(child.gameObject);

            while (queue.Count > 0)
                Destroy(queue.Dequeue());
        }
    }
}