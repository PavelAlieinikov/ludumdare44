﻿using Assets.AwesomeGame.Sources.God.Systems;
using AwesomeGame.Armies.Components;
using AwesomeGame.Edges.Components;
using Unity.Entities;
using UnityEngine;
using AwesomeGame.God.Components;
using AwesomeGame.God.Extensions;
using Unity.Collections;
using UnityEngine.SceneManagement;

namespace AwesomeGame
{
    public class GameManager
    {
        private static EntityQuery _query;
        private static EntityManager _entityManager;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void Start()
        {
            _entityManager = World.Active.EntityManager;
            _query = _entityManager.CreateEntityQuery(ComponentType.ReadWrite<Gosh>());
        }
        public static void StartGame()
        {
            _entityManager.CreateEntity(ComponentType.ReadWrite<Gosh>(), ComponentType.ReadWrite<MonthIncrement>());
            _query.SetSingleton(new Gosh());

            var goshSystem = World.Active.GetOrCreateSystem<GoshStateSystem>();
            goshSystem.Initialize();
        }

        public static void SwitchGameState(SessionState state)
        {
            if(_query == null || _query.CalculateLength() == 0)
                return;

            var gosh = _query.GetSingleton<Gosh>();
            gosh.State = state;
            _query.SetSingleton(gosh);

        }

        public static void DestroyGosh()
        {
            if (_query == null || _query.CalculateLength() == 0)
                return;

            var entity = _query.GetSingletonEntity();
            _entityManager.DestroyEntity(entity);


            _entityManager.Cleanup<ArmyView>();
            _entityManager.Cleanup<Transition>();
            Object.FindObjectOfType<IconsPool>()?.Cleanup();
            Debug.Log("Previous gosh was destroyed");
        }


    }
}
