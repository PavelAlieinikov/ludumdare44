﻿using System;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Systems;
using JetBrains.Annotations;
using Slash.Unity.DataBind.Core.Data;
using Slash.Unity.DataBind.Core.Presentation;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.UI
{
    public class UIRegion: Context
    {
        /*public void Curse()
        {
            var enemyRegionInteractions = World.Active.GetExistingSystem<RegionInteractionsSystem>();
            enemyRegionInteractions.Curse = Id;
        }*/
        
            /*var enemyRegionInteractions = World.Active.GetExistingSystem<RegionInteractionsSystem>();
            enemyRegionInteractions.Bless = Id;*/

        public void Interact()
        {
            this.OpenPopup();
        }

        private readonly Property<int> totalProperty = new Property<int>();
        public int Total
        {
            get
            {
                return this.totalProperty.Value;
            }
            set
            {
                this.totalProperty.Value = value;
            }
        }

        private readonly Property<Entity> idProperty = new Property<Entity>();
        public Entity Id
        {
            get
            {
                return this.idProperty.Value;
            }
            set
            {
                this.idProperty.Value = value;
            }
        }

        private readonly Property<Guid> rulerProperty = new Property<Guid>();
        public Guid Ruler
        {
            get
            {
                return this.rulerProperty.Value;
            }
            set
            {
                this.rulerProperty.Value = value;
            }
        }

        private readonly Property<bool> activeProperty = new Property<bool>();
        public bool Active
        {
            get
            {
                return this.activeProperty.Value;
            }
            set
            {
                this.activeProperty.Value = value;
            }
        }

        private readonly Property<Guid> playerProperty = new Property<Guid>();
        public Guid Player
        {
            get
            {
                return this.playerProperty.Value;
            }
            set
            {
                this.playerProperty.Value = value;
            }
        }

        private readonly Property<Guid> enemyProperty = new Property<Guid>(Guid.Empty);
        public Guid Enemy
        {
            get
            {
                return this.enemyProperty.Value;
            }
            set
            {
                this.enemyProperty.Value = value;
            }
        }

        private readonly Property<float> believersPercentProperty = new Property<float>();
        public float BelieversPercent
        {
            get
            {
                return believersPercentProperty.Value;
            }
            set
            {
                believersPercentProperty.Value = value;
            }
        }

        private readonly Property<float> hereticsPercentProperty = new Property<float>();
        public float HereticsPercent
        {
            get
            {
                return hereticsPercentProperty.Value;
            }
            set
            {
                hereticsPercentProperty.Value = value;
            }
        }

        private readonly Property<int> believersProperty = new Property<int>();
        public int Believers
        {
            get
            {
                return believersProperty.Value;
            }
            set
            {
                believersProperty.Value = value;
            }
        }

        private readonly Property<int> hereticsProperty = new Property<int>();
        public int Heretics
        {
            get
            {
                return hereticsProperty.Value;
            }
            set
            {
                hereticsProperty.Value = value;
            }
        }

        private readonly Property<float> faithProperty = new Property<float>();
        public float Faith
        {
            get
            {
                return faithProperty.Value;
            }
            set
            {
                faithProperty.Value = value;
            }
        }

        private readonly Property<bool> isEnemyProperty = new Property<bool>();
        public bool IsEnemy
        {
            get
            {
                return isEnemyProperty.Value;
            }
            set
            {
                isEnemyProperty.Value = value;
            }
        }

        private readonly Property<bool> isGoshProperty = new Property<bool>();
        public bool IsGosh
        {
            get
            {
                return isGoshProperty.Value;
            }
            set
            {
                isGoshProperty.Value = value;
            }
        }

        private readonly Property<bool> isLockedProperty = new Property<bool>();
        public bool IsLocked
        {
            get
            {
                return isLockedProperty.Value;
            }
            set
            {
                isLockedProperty.Value = value;
            }
        }
    }
}