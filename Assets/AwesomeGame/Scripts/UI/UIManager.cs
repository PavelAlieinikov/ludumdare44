﻿using AwesomeGame.God.Components;
using Slash.Unity.DataBind.Core.Data;
using TMPro;
using UnityEngine;

namespace AwesomeGame.UI
{
    public class UIManager : Context
    {
        public void NewGame()
        {
            GameManager.DestroyGosh();
            GameManager.StartGame();
            Debug.Log("Start game");
            MainMenuVisible = false;
            GameHudVisible = true;
            WinScreenVisible = false;
            LooseScreenVisible = false;
        }

        public void Credits()
        {
            CreditsVisible = !CreditsVisible;
            Debug.Log("Show credits");
        }

        public void Quit()
        {
            Debug.Log("Quit");
            Application.Quit();
        }

        public void OpenGameMenu()
        {
            GameMenuVisible = true;
            Debug.Log("Open Game menu");
            GameManager.SwitchGameState(SessionState.Pause);

        }

        public void ResumeGame()
        {
            GameMenuVisible = false;
            GameManager.SwitchGameState(SessionState.Playing);
        }

        public void ToMainMenu()
        {
            GameManager.DestroyGosh();
            MainMenuVisible = true;
            GameMenuVisible = false;
            GameHudVisible = false;
            WinScreenVisible = false;
            LooseScreenVisible = false;
        }

        #region Properties
        private readonly Property<bool> mainMenuVisibleProperty = new Property<bool>(true);
        public bool MainMenuVisible
        {
            get
            {
                return this.mainMenuVisibleProperty.Value;
            }
            set
            {
                this.mainMenuVisibleProperty.Value = value;
            }
        }

        private readonly Property<bool> creditsVisibleProperty = new Property<bool>(false);
        public bool CreditsVisible
        {
            get
            {
                return this.creditsVisibleProperty.Value;
            }
            set
            {
                this.creditsVisibleProperty.Value = value;
            }
        }

        private readonly Property<bool> winScreenVisibleProperty = new Property<bool>(false);
        public bool WinScreenVisible
        {
            get
            {
                return this.winScreenVisibleProperty.Value;
            }
            set
            {
                this.winScreenVisibleProperty.Value = value;
            }
        }

        private readonly Property<bool> looseScreenVisibleProperty = new Property<bool>(false);
        public bool LooseScreenVisible
        {
            get
            {
                return this.looseScreenVisibleProperty.Value;
            }
            set
            {
                this.looseScreenVisibleProperty.Value = value;
            }
        }

        private readonly Property<bool> gameHudVisibleProperty = new Property<bool>(false);
        public bool GameHudVisible
        {
            get
            {
                return this.gameHudVisibleProperty.Value;
            }
            set
            {
                this.gameHudVisibleProperty.Value = value;
            }
        }

        private readonly Property<bool> gameMenuVisibleProperty = new Property<bool>(false);
        public bool GameMenuVisible
        {
            get
            {
                return this.gameMenuVisibleProperty.Value;
            }
            set
            {
                this.gameMenuVisibleProperty.Value = value;
            }
        }

        private readonly Property<int> manaProperty = new Property<int>();
        public int Mana
        {
            get
            {
                return this.manaProperty.Value;
            }
            set
            {
                this.manaProperty.Value = value;
            }
        }

        private readonly Property<int> scoreProperty = new Property<int>();
        public int Score
        {
            get
            {
                return this.scoreProperty.Value;
            }
            set
            {
                this.scoreProperty.Value = value;
            }
        }

        #endregion


    }
}
