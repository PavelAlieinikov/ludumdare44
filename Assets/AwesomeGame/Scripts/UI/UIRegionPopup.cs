using JetBrains.Annotations;
using Slash.Unity.DataBind.Core.Data;
using UnityEngine;

namespace AwesomeGame.UI
{
    public class UIRegionPopup : Context
    {
        public enum ActionType
        {
            None,
            Curse,
            Bless,
            Close
        }

        [UsedImplicitly] //Will be executed from data binder
        public void OnCurse() => _action = ActionType.Curse;

        [UsedImplicitly] //Will be executed from data binder
        public void OnBless() => _action = ActionType.Bless;

        [UsedImplicitly] //Will be executed from data binder
        public void OnClose() => _action = ActionType.Close;

        private void Invalidate()
        {
            _action = ActionType.None;
            infoProperty.Value = string.Empty;
        }

        #region Properties

        private ActionType _action;

        public ActionType Action => _action;

        public bool IsConfigured => popupVisibleProperty.Value;

        private readonly Property<bool> popupVisibleProperty = new Property<bool>(false);

        public bool PopupVisible
        {
            get => popupVisibleProperty.Value;
            set
            {
                popupVisibleProperty.Value = value;
                Invalidate();
            }
        }

        private readonly Property<bool> blessVisibleProperty = new Property<bool>(false);

        public bool BlessVisible
        {
            get => blessVisibleProperty.Value;
            set => blessVisibleProperty.Value = value;
        }

        private readonly Property<bool> curseVisibleProperty = new Property<bool>(false);

        public bool CurseVisible
        {
            get => curseVisibleProperty.Value;
            set => curseVisibleProperty.Value = value;
        }

        private readonly Property<string> infoProperty = new Property<string>();

        public string Info
        {
            get => infoProperty.Value;
            set => infoProperty.Value = value;
        }

        private readonly Property<string> nameProperty = new Property<string>();

        public string Name
        {
            get => nameProperty.Value;
            set => nameProperty.Value = value;
        }

        private readonly Property<Sprite> iconProperty = new Property<Sprite>();

        public Sprite Icon
        {
            get => iconProperty.Value;
            set => iconProperty.Value = value;
        }

        private readonly Property<int> blessingCostProperty = new Property<int>();

        public int BlessingCost
        {
            get => blessingCostProperty.Value;
            set => blessingCostProperty.Value = value;
        }

        private readonly Property<int> curseCostProperty = new Property<int>();

        public int CurseCost
        {
            get => curseCostProperty.Value;
            set => curseCostProperty.Value = value;
        }

        #endregion
    }
}