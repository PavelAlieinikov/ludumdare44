using AwesomeGame.Regions.Components;
using Slash.Unity.DataBind.Core.Presentation;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.UI
{
    public static class UIExtension
    {
        public static void OpenPopup(this UIRegion uiRegion)
        {
            var entityQuery = World.Active.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<RegionPopup>());
            if (entityQuery.CalculateLength() > 0)
            {
                Debug.Log("Already opened!");
                return;
            }

            var hud = GameObject.FindGameObjectWithTag("RegionPopup").transform;
            var popup = (UIRegionPopup)hud.GetComponent<ContextHolder>().Context;

            var query = World.Active.EntityManager.CreateEntityQuery(ComponentType.ReadWrite<Region>());
            var nativeArray = query.ToEntityArray(Allocator.Persistent);
            foreach (var entity in nativeArray)
            {
                if (entity != uiRegion.Id)
                    continue;

                World.Active.EntityManager.AddSharedComponentData(entity, new RegionPopup{Value = popup});
                break;
            }
            nativeArray.Dispose();
        }
    }
}