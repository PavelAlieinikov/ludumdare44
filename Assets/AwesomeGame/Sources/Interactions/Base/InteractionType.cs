namespace AwesomeGame.Interactions.Base
{
    public enum InteractionType
    {
        Bless,
        Curse,
        Kill,
        Attack,
    }
}