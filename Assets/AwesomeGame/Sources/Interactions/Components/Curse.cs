﻿using Unity.Entities;

namespace AwesomeGame.Interactions.Components
{
    public struct Curse : IComponentData
    {
        public float HereticKillRatio;
        public float BeliverKillRatio;
    }
}