using System;
using Unity.Entities;
using Unity.Mathematics;

namespace AwesomeGame.Interactions.Components
{
    public struct Attack : IComponentData
    {
        public int Migrants;
        public Guid Owner;
        public Entity Source;
        public bool Believers;

        public float3 TargetPosition;
    }
}