﻿using Unity.Entities;

namespace AwesomeGame.Interactions.Components
{
    public struct Bless: IComponentData
    {
        public float Value;
    }
}