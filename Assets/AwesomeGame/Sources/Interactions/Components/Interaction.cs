using Unity.Entities;

namespace AwesomeGame.Interactions.Components
{
    public struct Interaction : IComponentData
    {
        public Entity Target;
    }
}