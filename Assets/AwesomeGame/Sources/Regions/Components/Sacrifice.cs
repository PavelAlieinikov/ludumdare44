﻿using System;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct Sacrifice : IComponentData, ICronoComponent
    {
        public float BeliversRequire;
        public float ManaConvertionRatio;

        [SerializeField]
        private float _incrementTime;

        public float IncrementTime => _incrementTime;
        public float Timer { get; set; }
    }
}