﻿using System;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct RegionInfluence
        : IComponentData
    {
        public float BlessingPower;
        public int BlessingCost;

        public float CurseBeliverKillRatio;
        public float CurseHereticKillRatio;
        public int CurseCost;


        public float AttackBlessingPower;

        public float ConversionRation;
    }
}