using AwesomeGame.UI;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    public struct RegionPopup : ISharedComponentData
    {
        public UIRegionPopup Value;
    }
}