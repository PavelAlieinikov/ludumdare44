namespace AwesomeGame.Regions.Components
{
    public interface ICronoComponent
    {
        float IncrementTime { get; }
        float Timer{ get; set; }
    }
}