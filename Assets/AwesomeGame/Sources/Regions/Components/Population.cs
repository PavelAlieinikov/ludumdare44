using System;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct Population : IComponentData
    {
        public int Believers;
        public int Heretics;
    }
}