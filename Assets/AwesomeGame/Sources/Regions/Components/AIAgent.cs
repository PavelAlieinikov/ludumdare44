using System;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct AIAgent : IComponentData, ICronoComponent
    {
        public bool IsActive;
        public float FaithRequire;
        public float Chance;

        public float MigrationRation;
        public float MigrationTime;

        [SerializeField]
        private float _incrementTime;

        public float IncrementTime => _incrementTime;
        public float Timer { get; set; }
        public int MinPopulation;
    }
}