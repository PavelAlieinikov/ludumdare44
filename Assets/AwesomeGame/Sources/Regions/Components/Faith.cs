﻿using System;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct Faith : IComponentData
    {
        public float Value;
    }
}