using System;
using UnityEngine;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct RegionConsumption : IIncrementComponent
    {
        [SerializeField]
        private float _incrementTime;
        [SerializeField]
        public bool _percent;

        [SerializeField]
        private float _increment;

        public float IncrementTime => _incrementTime;
        public float Increment => _increment;
        public bool Percent => _percent;

        public float Timer { get; set; }
    }
}