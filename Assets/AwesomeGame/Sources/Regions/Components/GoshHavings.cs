using System;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    public struct GoshHavings : ISharedComponentData
    {
        public Guid Gosh;
    }
}