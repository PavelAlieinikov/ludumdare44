using System;
using System.Runtime.InteropServices;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    [Serializable]
    public struct Region : IComponentData
    {
        public byte Value;
    }
}