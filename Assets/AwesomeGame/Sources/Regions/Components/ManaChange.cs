﻿using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    public struct ManaChange : IComponentData
    {
        public int Value;
    }
}