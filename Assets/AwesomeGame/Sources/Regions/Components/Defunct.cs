using System.Runtime.InteropServices;
using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public struct Defunct : IComponentData
    {

    }
}