﻿using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    public struct IconTimer : ICronoComponent, IComponentData
    {
        public float IncrementTime => 1f;
        public float Timer { get; set; }
    }
}