using Unity.Entities;

namespace AwesomeGame.Regions.Components
{
    public interface IIncrementComponent : IComponentData, ICronoComponent
    {
        float Increment{ get; }
        bool Percent { get; }
    }
}