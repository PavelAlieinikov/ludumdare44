using AwesomeGame.Regions.Components;
using UnityEngine;

namespace AwesomeGame.Regions.Extensions
{
    public static class IncrementExtension
    {
        public static int Apply<TIncrement>(this int value, ref TIncrement increment)
            where TIncrement : struct, IIncrementComponent
        {
            return !ChronoUtils.IsTicked(ref increment) ? value : increment.GetValue(value);
        }

        public static int GetValue<TIncrement>(this TIncrement increment, int value)
            where TIncrement : struct, IIncrementComponent
        {
            var result = increment.Percent
                ? Mathf.CeilToInt(value +  value * increment.Increment)
                : value + (int)increment.Increment;

            if (result == value)
                result++;
            return result;
        }

        public static float Apply<TIncrement>(this float value, ref TIncrement increment)
            where TIncrement : struct, IIncrementComponent
        {
            if (!ChronoUtils.IsTicked(ref increment))
                return value;

            var result = increment.Percent
                             ? value + value * increment.Increment
                             : value + increment.Increment;

            result = Mathf.Clamp01(result);
            return result;
        }
    }
}