using AwesomeGame.Regions.Components;
using UnityEngine;

namespace AwesomeGame.Regions.Extensions
{
    public class ChronoUtils
    {
        public static bool IsTicked<TIncrement>(ref TIncrement component)
            where TIncrement : ICronoComponent
        {
            component.Timer -= Time.deltaTime;
            if (component.Timer > 0)
                return false;

            component.Timer = component.IncrementTime;
            return true;
        }

        public static void Init<TIncrement>(ref TIncrement component)
            where TIncrement : ICronoComponent
        {
            component.Timer = component.IncrementTime;
        }
    }
}