using System;
using AwesomeGame.Regions.Components;
using Unity.Entities;

namespace AwesomeGame.Regions.Extensions
{
    public static class PopulationExtension
    {

        public static int Total(this Population population) =>
            population.Heretics + population.Believers;

        public static Guid GetOwnerOf(this EntityManager manager, Entity entity) =>
            manager.HasComponent<GoshHavings>(entity)
                ? manager.GetSharedComponentData<GoshHavings>(entity).Gosh
                : Guid.Empty;
    }
}