﻿using System.Collections.Generic;
using AwesomeGame.Edges.Components;
using AwesomeGame.Edges.Extensions;
using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    public class CurseRegionSystem : ComponentSystem
    {
        private EntityQuery _query;
        private EntityQuery _edgesQuery;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(), ComponentType.ReadWrite<Population>(), ComponentType.ReadOnly<Curse>());
            _edgesQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Edge>());
        }

        protected override void OnUpdate()
        {
            Entities.With(_query)
                .ForEach((Entity entity, ref Population population, ref Curse curse) =>
                    {
                        PostUpdateCommands.RemoveComponent<Curse>(entity);

                        var beliversKilled = Mathf.CeilToInt(population.Believers * curse.BeliverKillRatio);
                        population.Believers -= beliversKilled;

                        var hereticsKilled = Mathf.CeilToInt(population.Heretics * curse.HereticKillRatio);
                        population.Heretics -= hereticsKilled;

                        var totalKilled = beliversKilled + hereticsKilled;

                        var animator = EntityManager.GetComponentObject<RegionAnimationPlayer>(entity);
                        animator.PlayCurse();

                        SpreadBless(entity, totalKilled);
                    });
        }

        private void SpreadBless(Entity entity, int totalKilled)
        {
            var targets = new List<Entity>();

            Entities.With(_edgesQuery)
                .ForEach((Entity edgeEntity, ref Edge edge) =>
                    {
                        if (!edge.TryGetNeighborOf(entity, out Entity neighbor))
                            return;

                        if (EntityManager.HasComponent(neighbor, ComponentType.ReadOnly<GoshHavings>()))
                        {
                            targets.Add(neighbor);
                        }
                    });

            var config = EntityManager.GetComponentData<RegionInfluence>(entity);
            foreach (Entity target in targets)
            {
                var transitionEntity = PostUpdateCommands.CreateEntity();
                PostUpdateCommands.AddComponent(transitionEntity, new Transition { Time = 1, Target = target, Source = entity, State = TransitionState.InTransit});
                PostUpdateCommands.AddComponent(transitionEntity, new Bless { Value = (totalKilled / (float)100) * config.BlessingPower });
            }
        }
    }
}