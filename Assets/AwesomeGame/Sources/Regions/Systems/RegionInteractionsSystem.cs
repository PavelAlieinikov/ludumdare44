using System;
using AwesomeGame.God.Components;
using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class RegionInteractionsSystem : ComponentSystem
    {
        private EntityQuery _query;

        public Entity Curse;

        public Entity Bless;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(typeof(Region));
        }

        protected override void OnUpdate()
        {
            if (_query.CalculateLength() == 0)
                return;

            Entities.With(_query)
                .ForEach((Entity entity, ref Region region) =>
                {
                    if (entity == Curse)
                    {
                        CurseInteraction(entity);
                        Curse = Entity.Null;
                    }

                    if (entity == Bless)
                    {
                        BlessInteraction(entity);
                        Bless = Entity.Null;
                    }

                });
        }

        private void CurseInteraction(Entity entity)
        {
            if (!HasSingleton<Gosh>())
                return;

            var gosh = GetSingletonEntity<Gosh>();
            var goshData = EntityManager.GetComponentData<Gosh>(gosh);

            var config = EntityManager.GetComponentData<RegionInfluence>(entity);
            if (goshData.Mana < config.CurseCost)
            {
                Debug.LogWarning("Not enough minerals");
                return;
            }

            PostUpdateCommands.AddComponent(entity, new Curse {BeliverKillRatio = config.CurseBeliverKillRatio
                , HereticKillRatio = config.CurseHereticKillRatio});
            PostUpdateCommands.AddComponent(entity, new ManaChange { Value = -config.CurseCost });
        }

        private void BlessInteraction(Entity entity)
        {
            if (!HasSingleton<Gosh>())
                return;

            var gosh = GetSingletonEntity<Gosh>();
            var goshData = EntityManager.GetComponentData<Gosh>(gosh);
            var config = EntityManager.GetComponentData<RegionInfluence>(entity);
            if (goshData.Mana < config.BlessingCost)
            {
                Debug.LogWarning("Not enough minerals");
                return;
            }

            PostUpdateCommands.AddComponent(entity, new Bless{Value = config.BlessingPower} );
            PostUpdateCommands.AddComponent(entity, new ManaChange{Value = -config.BlessingCost});
        }
    }
}