﻿using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using Unity.Entities;
using Unity.Mathematics;

namespace AwesomeGame.Regions.Systems
{
    public class BlessRegionSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(
                ComponentType.ReadWrite<Faith>(),
                ComponentType.ReadWrite<Population>(),
                ComponentType.ReadOnly<Bless>(),
                ComponentType.ReadOnly<Region>());
        }

        protected override void OnUpdate()
        {
            Entities.With(_query)
                .ForEach(
                    (Entity entity, ref Faith faith, ref Bless bless, ref Region region, ref RegionInfluence influence,
                        ref Population population) =>
                    {
                        var animator = EntityManager.GetComponentObject<RegionAnimationPlayer>(entity);
                        animator.PlayBless();

                        faith.Value += bless.Value;
                        var blessed = (int) math.ceil(population.Heretics * influence.ConversionRation);
                        population.Believers += blessed;
                        population.Heretics -= blessed;

                        PostUpdateCommands.RemoveComponent<Bless>(entity);
                    });
        }
    }
}