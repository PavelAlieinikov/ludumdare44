using System;
using AwesomeGame.God.Components;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class InitRegions : ComponentSystem
    {
        private Guid _lastGosh;

        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(),
                                                     ComponentType.ReadOnly<RegionConfig>(),
                                                     ComponentType.ReadWrite<Population>());
        }

        protected override void OnUpdate()
        {
            if (!HasSingleton<Gosh>())
                return;

            var gosh = GetSingleton<Gosh>().Guid;
            if(gosh == _lastGosh)
                return;


            var goshHavings = new GoshHavings{Gosh = gosh};
            Entities.With(_query)
                .ForEach((Entity entity, RegionConfig config, ref Population population,
                             ref Region region, ref Faith faith, ref  AIAgent agent) =>
                {
                    ChronoUtils.Init(ref agent);
                    population.Believers = config.StartBelievers;
                    population.Heretics = config.StartHeretics;
                    faith.Value = config.StartFaith;
                    agent.IsActive = config.AIActiveOnStart;

                    if (population.Believers > 0)
                    {
                        if(!EntityManager.HasComponent<GoshHavings>(entity))
                            PostUpdateCommands.AddSharedComponent(entity, goshHavings);
                        else
                            PostUpdateCommands.SetSharedComponent(entity, goshHavings);
                    }
                    else if(population.Believers <= 0 && EntityManager.HasComponent<GoshHavings>(entity))
                        PostUpdateCommands.RemoveComponent<GoshHavings>(entity);

                    if(EntityManager.HasComponent<Defunct>(entity))
                        PostUpdateCommands.RemoveComponent<Defunct>(entity);
                });

            _lastGosh = gosh;
        }
    }
}