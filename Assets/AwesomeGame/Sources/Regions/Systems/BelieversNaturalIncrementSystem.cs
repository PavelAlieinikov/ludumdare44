using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(HereticsNaturalIncrementSystem))]
    public class BelieversNaturalIncrementSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadWrite<Population>(),
                                                     ComponentType.ReadOnly<GoshHavings>(),
                                                     ComponentType.ReadOnly<Faith>(),
                                                     ComponentType.ReadOnly<BelieversIncrement>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_query)
                .ForEach((ref Population population, ref BelieversIncrement increment, ref Faith faith) =>
                {
                    if (!ChronoUtils.IsTicked(ref increment))
                        return;

                    if(faith.Value <= 0)
                        return;

                    var percent = increment.Increment * faith.Value;/* + 0.05F*/;
                    var count = Mathf.CeilToInt(population.Heretics * percent);

                    if (population.Heretics < count)
                        count = population.Heretics;

                    population.Believers = population.Believers + count;
                    population.Heretics = population.Heretics - count;
                });
        }
    }
}