using System;
using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(IncommingAttackSystem))]
    public class AttackRegionSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {

            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(),
                                                     ComponentType.ReadWrite<Population>(),
                                                     ComponentType.ReadOnly<Attack>());
        }

        private IconsPool _iconsPool;
        protected override void OnStartRunning()
        {
            _iconsPool = Object.FindObjectOfType<IconsPool>();
        }

        protected override void OnUpdate()
        {
            if (_query.CalculateLength() == 0)
                return;

            var entities = _query.ToEntityArray(Allocator.Persistent);
            foreach (var entity in entities)
            {
                var attack = EntityManager.GetComponentData<Attack>(entity);
                var population = EntityManager.GetComponentData<Population>(entity);

                EntityManager.RemoveComponent<Attack>(entity);
                var owner = EntityManager.GetOwnerOf(entity);

                if (owner == attack.Owner)
                {
                    Migrate(population, attack, entity);
                    continue;
                }

                if(owner == Guid.Empty)
                    GoshVsEnemyFight(population, attack, entity);
                else
                    EnemyVsGoshFight(population, attack, entity);
            }

            entities.Dispose();
        }



        private void Migrate(Population population, Attack attack, Entity entity)
        {
            if(attack.Believers)
                population.Believers += attack.Migrants;
            else
                population.Heretics += attack.Migrants;

            EntityManager.SetComponentData(entity, population);
//            CreateDisplayResult(IconType.Migration, attack.TargetPosition);
        }

        private void EnemyVsGoshFight(Population population, Attack attack, Entity entity)
        {
            var win = false;
            population.Believers -= attack.Migrants;
            if (population.Believers < 0)
            {
                population.Heretics += population.Believers;
                population.Believers = 0;
                if (population.Heretics < 0)
                {
                    population.Heretics *= -1;//Convert rest to population
                    win = true;
                }

            }

            EntityManager.SetComponentData(entity, population);

            if (win)
            {
                EntityManager.RemoveComponent<GoshHavings>(entity);
                EntityManager.SetComponentData(entity, new Faith());
                CreateDisplayResult(IconType.Defeat, attack.TargetPosition);
                Debug.Log($"Region {entity} was lost!");
                return;
            }

            CreateDisplayResult(IconType.Victory, attack.TargetPosition);
            Debug.Log($"Attack on Region {entity} was weaned");

        }
        private void GoshVsEnemyFight(Population population, Attack attack, Entity entity)
        {
            var win = false;

            population.Heretics -= attack.Migrants;
            if (population.Heretics < 0)
            {
                population.Believers = population.Heretics * -1; //Convert rest to believers
                population.Heretics = 0;
                win = true;
            }
            EntityManager.SetComponentData(entity, population);

            if (win)
            {
                EntityManager.AddSharedComponentData(entity, new GoshHavings {Gosh = attack.Owner});
                CreateDisplayResult(IconType.Victory, attack.TargetPosition);
                Debug.Log($"Region {entity} was captured");

                //Send bless
                var config = EntityManager.GetComponentData<RegionInfluence>(entity);
                EntityManager.AddComponentData(attack.Source, new Bless {Value = config.AttackBlessingPower});
                return;
            }

            Debug.Log($"Region {entity} not captured!");
            CreateDisplayResult(IconType.Defeat, attack.TargetPosition);
        }

        private void CreateDisplayResult(IconType iconType, float3 targetPosition)
        {
            var transform = _iconsPool.Create(iconType);
            var iconEntity = EntityManager.CreateEntity();
            EntityManager.AddComponentData(iconEntity, new IconTimer{ Timer = 1.5f });
            EntityManager.AddComponentData(iconEntity, new Translation{Value = targetPosition});
            EntityManager.AddComponentObject(iconEntity, transform);
            transform.position = targetPosition;
        }


    }
}