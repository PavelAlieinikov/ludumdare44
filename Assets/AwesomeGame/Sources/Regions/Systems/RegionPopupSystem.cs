using AwesomeGame.Edges.Components;
using AwesomeGame.Edges.Extensions;
using AwesomeGame.God.Components;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using AwesomeGame.UI;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class RegionPopupSystem : ComponentSystem
    {
        private EntityQuery _query;
        private RegionInteractionsSystem _interactions;
        private EntityQuery _edgesQuery;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(),
                                                     ComponentType.ReadOnly<RegionPopup>());
            _query.SetFilterChanged(ComponentType.ReadOnly<Region>());
            _edgesQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Edge>());

            _interactions = World.Active.GetExistingSystem<RegionInteractionsSystem>();
        }

        protected override void OnUpdate()
        {
            if (_query.CalculateLength() == 0)
                return;

            Entities.With(_query)
                .ForEach((Entity entity, RegionPopup popup, ref Region region) =>
                {
                    ConfigurePopup(entity, popup.Value);
                    if (!popup.Value.IsConfigured)
                        popup.Value.PopupVisible = true;

                    var actionHandled = ActionHandler(entity, popup.Value);
                    if (!actionHandled)
                        return;

                    PostUpdateCommands.RemoveComponent<RegionPopup>(entity);
                    popup.Value.PopupVisible = false;
                });
        }

        private bool ActionHandler(Entity entity, UIRegionPopup popup)
        {
            switch (popup.Action)
            {
                //TODO: create an interaction entity will be more correctly
                case UIRegionPopup.ActionType.Curse:
                    _interactions.Curse = entity;
                    break;
                case UIRegionPopup.ActionType.Bless:
                    _interactions.Bless = entity;
                    break;
                case UIRegionPopup.ActionType.Close:
                    break;
                default:
                    return false;
            }

            return true;
        }

        private void ConfigurePopup(Entity entity, UIRegionPopup popup)
        {
            var regionInfo = EntityManager.GetComponentObject<RegionInfo>(entity);
            var population = EntityManager.GetComponentData<Population>(entity);
            var hereticsIncrement = EntityManager.GetComponentData<HereticsIncrement>(entity);
            var config = EntityManager.GetComponentData<RegionInfluence>(entity);
            var faith = EntityManager.GetComponentData<Faith>(entity);
            var agent = EntityManager.GetComponentData<AIAgent>(entity);

            var info =
                $"Population + <b>{hereticsIncrement.GetValue(population.Heretics)}</b> per {hereticsIncrement.IncrementTime} month \n"
                + $"Believers <b>{population.Believers}</b>\nHeretics <b>{population.Heretics}</b>\n"
                + $"Faith <b>{Mathf.CeilToInt(faith.Value * 100)}%</b>\n";
            info += $"Attack when faith > <b>{Mathf.CeilToInt(agent.FaithRequire * 100)}%</b>\n\n";
            info += $"Blessing cost <b>{config.BlessingCost} life</b>\n";
            info += $"Curse cost <b>{config.CurseCost} life</b>\n";

            popup.Info = info;
            popup.BlessVisible = CanBeBlessed(entity, config);
            popup.CurseVisible = CanBeCursed(entity, config);
            popup.BlessingCost = config.BlessingCost;
            popup.CurseCost = config.CurseCost;

            popup.Icon = regionInfo.Icon;
            popup.Name = regionInfo.Name;
        }

        private bool CanBeBlessed(Entity entity, RegionInfluence config)
        {
            var gosh = GetSingletonEntity<Gosh>();
            var goshData = EntityManager.GetComponentData<Gosh>(gosh);
            return goshData.Mana >= config.BlessingCost && EntityManager.HasComponent<GoshHavings>(entity);
        }

        private bool CanBeCursed(Entity entity, RegionInfluence config)
        {
            var gosh = GetSingletonEntity<Gosh>();
            var goshData = EntityManager.GetComponentData<Gosh>(gosh);
            if (goshData.Mana < config.CurseCost)
                return false;

            if (EntityManager.HasComponent<GoshHavings>(entity))
                return true;

            var result = false;
            Entities.With(_edgesQuery).ForEach((Entity edgeEntity, ref Edge edge) =>
            {
                if(result)
                    return;

                Entity neighbor;
                if (!edge.TryGetNeighborOf(entity, out neighbor))
                    return;

                result = EntityManager.HasComponent<GoshHavings>(neighbor);
            });

            return result;
        }
    }
}