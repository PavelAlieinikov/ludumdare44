﻿using System.Collections.Generic;
using AwesomeGame.Edges.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class IncommingAttackSystem : ComponentSystem
    {
        private EntityQuery _query;
        private Dictionary<Entity, Attack> _attackBuffer;

        protected override void OnCreateManager()
        {
            _attackBuffer = new Dictionary<Entity, Attack>();
            _query = EntityManager.CreateEntityQuery(
                ComponentType.ReadOnly<Attack>(),
                ComponentType.ReadOnly<Transition>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_query)
                .ForEach(
                    (Entity entity, ref Transition transition, ref Attack attack) =>
                    {
                        if (transition.State != TransitionState.ReadyForProcessing)
                            return;

                        var target = transition.Target;

                        if (!EntityManager.HasComponent<Region>(target))
                        {
                            Debug.LogError($"Wrong transition target: {target}");
                            return;
                        }

                        if (!_attackBuffer.ContainsKey(target))
                            _attackBuffer.Add(target, attack);
                        else
                        {
                            attack.Migrants += _attackBuffer[target].Migrants;
                            _attackBuffer[target] = attack;
                        }

                        transition.State = TransitionState.Processed;
                    });

            foreach (var attack in _attackBuffer)
            {
                if (!EntityManager.Exists(attack.Key))
                {
                    Debug.LogError($"Entity does not exist: {attack.Key}");
                    continue;
                }

                if (!EntityManager.HasComponent<Region>(attack.Key))
                {
                    Debug.LogError($"Transition was sent not to region {attack.Key}!");
                    continue;
                }

                if (EntityManager.HasComponent<Attack>(attack.Key))
                {
                    Debug.LogError($"Entity already has Attack: {attack.Key}");
                    continue;
                }

                EntityManager.AddComponentData(attack.Key, new Attack
                {
                    Owner = attack.Value.Owner,
                    Source = attack.Value.Source,
                    Migrants = attack.Value.Migrants,
                    TargetPosition = attack.Value.TargetPosition
                });
            }

            _attackBuffer.Clear();
        }
    }
}