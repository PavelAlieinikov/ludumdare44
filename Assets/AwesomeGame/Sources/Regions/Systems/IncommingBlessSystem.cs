﻿using System.Collections.Generic;
using AwesomeGame.Edges.Components;
using AwesomeGame.Interactions.Components;
using JetBrains.Annotations;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class IncommingBlessSystem : ComponentSystem
    {
        private EntityQuery _transitionQuery;
        private Dictionary<Entity, float> _regionFaith;

        protected override void OnCreateManager()
        {
            _regionFaith = new Dictionary<Entity, float>();
            _transitionQuery = EntityManager.CreateEntityQuery(
                ComponentType.ReadOnly<Transition>(),
                ComponentType.ReadOnly<Bless>());
        }

        protected override void OnUpdate()
        {
            Entities.With(_transitionQuery).ForEach(
                (Entity entity, ref Transition transition, ref Bless bless) =>
                {
                    if (transition.State != TransitionState.ReadyForProcessing)
                        return;

                    if (_regionFaith.ContainsKey(transition.Target))
                        _regionFaith[transition.Target] += bless.Value;
                    else
                        _regionFaith.Add(transition.Target, bless.Value);

                    transition.State = TransitionState.Processed;
                });

            foreach (var faith in _regionFaith)
                EntityManager.AddComponentData(faith.Key, new Bless {Value = faith.Value});
            _regionFaith.Clear();
        }
    }
}