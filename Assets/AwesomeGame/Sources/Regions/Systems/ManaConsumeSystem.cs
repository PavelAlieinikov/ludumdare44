﻿using AwesomeGame.God.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    public class ManaConsumeSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(),
                                                     ComponentType.ReadOnly<ManaChange>());
        }

        protected override void OnUpdate()
        {
            int value = 0;
            Entities.With(_query).ForEach((Entity entity, ref ManaChange manaConsume) =>
            {
                
                value += manaConsume.Value;

                PostUpdateCommands.RemoveComponent<ManaChange>(entity);
            });

            if (!this.IsPlaying())
                return;

            var gosh = GetSingletonEntity<Gosh>();
            var goshData = EntityManager.GetComponentData<Gosh>(gosh);
            goshData.Mana += value;
            PostUpdateCommands.SetComponent(gosh, goshData);
        }
    }
}