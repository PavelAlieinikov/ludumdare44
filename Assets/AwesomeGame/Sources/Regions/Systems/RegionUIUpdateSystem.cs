﻿using System;
using Assets.AwesomeGame.Sources.God.Systems;
using AwesomeGame.God.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Extensions;
using AwesomeGame.UI;
using JetBrains.Annotations;
using Slash.Unity.DataBind.Core.Presentation;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Components
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class RegionUIUpdateSystem : ComponentSystem
    {
        private EntityQuery _regionsQuery;

        protected override void OnCreateManager()
        {
            _regionsQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_regionsQuery).ForEach(
                (Entity entity, ref Region region, ref AIAgent agent, ref Population population, ref Faith faith) =>
                {
                    var transform = EntityManager.GetComponentObject<Transform>(entity);
                    var context = transform.GetComponentInChildren<ContextHolder>();
                    var uiRegion = (UIRegion)context.Context;
                    uiRegion.Believers = population.Believers;
                    uiRegion.Total = population.Believers + population.Heretics;
                    uiRegion.Heretics = population.Heretics;
                    uiRegion.BelieversPercent = uiRegion.Total == 0 ? 0 : ((float)population.Believers / (float)uiRegion.Total);
                    uiRegion.HereticsPercent = uiRegion.Total == 0 ? 0 : 1 - ((float)population.Believers / (float)uiRegion.Total);
                    uiRegion.Id = entity;
                    uiRegion.Faith = faith.Value;

                    if (EntityManager.HasComponent<GoshHavings>(entity))
                    {
                        var ruler = EntityManager.GetSharedComponentData<GoshHavings>(entity);
                        uiRegion.Ruler = ruler.Gosh;
                    }
                    else
                    {
                        uiRegion.Ruler = Guid.Empty;
                    }

                    uiRegion.IsLocked = !agent.IsActive;

                    if (!HasSingleton<Gosh>())
                    {
                        uiRegion.IsLocked = true;
                        return;
                    }

                    var player = GetSingletonEntity<Gosh>();
                    uiRegion.Player = EntityManager.GetComponentData<Gosh>(player).Guid;
                    uiRegion.IsGosh = !uiRegion.IsLocked && uiRegion.Player == uiRegion.Ruler;
                    uiRegion.IsEnemy = !uiRegion.IsLocked && !uiRegion.IsGosh;
                });
        }
    }
}