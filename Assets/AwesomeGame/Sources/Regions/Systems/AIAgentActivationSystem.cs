using System.Linq;
using AwesomeGame.Edges.Components;
using AwesomeGame.Edges.Extensions;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class AIAgentActivationSystem : ComponentSystem
    {
        private EntityQuery _query;
        private EntityQuery _edgesQuery;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(),
                                                     ComponentType.ReadOnly<AIAgent>(),
                                                     ComponentType.Exclude<GoshHavings>());

            _edgesQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Edge>());
        }

        protected override void OnUpdate()
        {
            if(!this.IsPlaying() && _query.CalculateLength() == 0)
                return;


            var entities = _query.ToEntityArray(Allocator.Persistent);
            var edgeEntities = _edgesQuery.ToEntityArray(Allocator.Persistent);

            foreach (var entity in entities)
            {
                var agent = EntityManager.GetComponentData<AIAgent>(entity);
                if(agent.IsActive)
                    continue;

                var owner = EntityManager.GetOwnerOf(entity);
                var neighbors = edgeEntities.GetNeighborsOf(entity);
                var activate = neighbors.Any(e => EntityManager.GetOwnerOf(e) != owner);
                if (!activate)
                    continue;

                agent.IsActive = true;
                EntityManager.SetComponentData(entity, agent);

            }

            edgeEntities.Dispose();
            entities.Dispose();
        }
    }
}