﻿using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    public class MovementResultDisplaySystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(
                ComponentType.ReadWrite<IconTimer>(),
                ComponentType.ReadWrite<Translation>(), 
                ComponentType.ReadOnly<Transform>());
        }

        protected override void OnUpdate()
        {
            Entities.With(_query).ForEach(
                (Entity entity, Transform transform, ref IconTimer iconTimer, ref Translation translation) =>
                {
                    if (!ChronoUtils.IsTicked(ref iconTimer))
                    {
                        translation.Value = translation.Value + new float3(0f, 0.05f, 0f) * Time.deltaTime;
                        transform.position = translation.Value;
                    }
                    else
                    {
                        Object.Destroy(transform.gameObject);
                        PostUpdateCommands.DestroyEntity(entity);
                    }
                });
        }
    }
}