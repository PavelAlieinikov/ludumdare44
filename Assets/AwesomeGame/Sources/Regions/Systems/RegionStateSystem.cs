using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class RegionStateSystem : ComponentSystem
    {
        private EntityQuery _populationQuery;

        protected override void OnCreateManager()
        {
            var populationType = ComponentType.ReadOnly<Population>();
            var populationArchetype = new[]
            {
                populationType,
                ComponentType.ReadOnly<Region>()
            };

            _populationQuery = EntityManager.CreateEntityQuery(populationArchetype);
            _populationQuery.SetFilterChanged(populationType);
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_populationQuery)
                .ForEach((Entity entity, ref Population population) =>
                {
                    if (population.Total() > 0)
                    {
                        if(EntityManager.HasComponent<Defunct>(entity))
                            PostUpdateCommands.RemoveComponent<Defunct>(entity);
                        return;
                    }

                    PostUpdateCommands.AddComponent(entity, new Defunct());
//                    Debug.Log($"Region {entity} was defunct");
                });
        }
    }
}