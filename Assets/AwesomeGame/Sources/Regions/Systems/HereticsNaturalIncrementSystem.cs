using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class HereticsNaturalIncrementSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadWrite<Population>(),
                                                     ComponentType.ReadOnly<HereticsIncrement>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_query).ForEach((ref Population population, ref HereticsIncrement increment) =>
            {
                if( population.Heretics < 0 && population.Believers < 0)
                    return;

                population.Heretics = population.Heretics.Apply(ref increment);
            });
        }
    }
}