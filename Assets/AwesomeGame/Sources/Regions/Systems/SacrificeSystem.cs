﻿using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace AwesomeGame.Regions.Systems
{
    public class SacrificeSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(
                ComponentType.ReadOnly<Sacrifice>(),
                ComponentType.ReadWrite<Population>(),
                ComponentType.ReadOnly<GoshHavings>(),
                ComponentType.ReadOnly<Region>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_query).ForEach(
                (Entity entity, ref Sacrifice sacrifice, ref Population population) =>
                {

                    if (population.Believers <= Mathf.CeilToInt(population.Believers * sacrifice.BeliversRequire))
                        return;

                    if (EntityManager.HasComponent(entity, ComponentType.ReadOnly<ManaChange>())) //Dirtiest hack?
                        return;

                    if (!ChronoUtils.IsTicked(ref sacrifice))
                        return;

                    var sacrificialCount = Mathf.CeilToInt(population.Believers * sacrifice.BeliversRequire);

                    var animator = EntityManager.GetComponentObject<RegionAnimationPlayer>(entity);
                    animator.PlayBlood();

                    var manaGained = Mathf.FloorToInt(sacrificialCount * sacrifice.ManaConvertionRatio);
                    PostUpdateCommands.AddComponent(entity, new ManaChange{Value = manaGained});
                });
        }
    }
}