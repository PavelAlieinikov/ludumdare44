using AwesomeGame.God.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class RegionManaConsumptionSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            var archetype = new[]
            {
                ComponentType.ReadOnly<Region>(),
                ComponentType.ReadOnly<RegionConsumption>(),
                ComponentType.ReadOnly<Population>(),
                ComponentType.ReadOnly<GoshHavings>()
            };
            _query = EntityManager.CreateEntityQuery(archetype);
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            var value = 0F;

            Entities.With(_query)
                .ForEach((Entity entity, ref RegionConsumption consumption, ref Population population) =>
                {
                    if(!ChronoUtils.IsTicked(ref consumption))
                        return;

                    var total = population.Total();
                    if(total <= 0)
                        return;

                    var p = 1 - population.Believers / total;
                    value += p * consumption.Increment;
                });

            var gosh = GetSingleton<Gosh>();
            gosh.Mana -= (int)value;
            if (gosh.Mana < 0)
                gosh.Mana = 0;
            SetSingleton(gosh);
        }
    }
}