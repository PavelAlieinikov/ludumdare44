﻿using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using Unity.Entities;

namespace AwesomeGame.Regions.Systems
{
    public class FaithDecreaseSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadWrite<Faith>(),
                                                     ComponentType.ReadOnly<FaithDecrement>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_query).ForEach((ref Faith faith, ref FaithDecrement decrement) =>
            {
                if (faith.Value < 0 )
                    return;

                faith.Value = faith.Value.Apply(ref decrement);
            });
        }
    }
}