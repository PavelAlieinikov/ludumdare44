using System.Collections.Generic;
using AwesomeGame.Edges.Components;
using AwesomeGame.Edges.Systems;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace AwesomeGame.Edges.Extensions
{
    public static class EdgeExtension
    {
        public static bool TryGetNeighborOf(this Edge edge, Entity node, out Entity neighbor)
        {
            neighbor = Entity.Null;
            if (edge.A == node)
            {
                neighbor = edge.B;
                return true;
            }

            if (edge.B != node)
                return false;

            neighbor = edge.A;
            return true;
        }

        public static float3x2 GetEdgePoints(this Entity edge, Entity node)
        {
            var data = World.Active.EntityManager.GetComponentData<Edge>(edge);
            var points = World.Active.EntityManager.GetComponentData<EdgePoints>(edge).Value;
            return new float3x2(
                data.A == node ? points.c0 : points.c1,
                data.A == node ? points.c1 : points.c0
            );
        }


        public static Entity FindEdge(this NativeArray<Entity> edgeEntities, Entity sourceRegion, Entity targetRegion)
        {
            var length = edgeEntities.Length;
            for (var i = 0; i < length; i++)
            {
                var edge = World.Active.EntityManager.GetComponentData<Edge>(edgeEntities[i]);
                if (edge.A == sourceRegion && edge.B == targetRegion)
                    return edgeEntities[i];
                if (edge.B == sourceRegion && edge.A == targetRegion)
                    return edgeEntities[i];
            }

            return Entity.Null;
        }

        public static IEnumerable<Entity> GetNeighborsOf(this NativeArray<Entity> edgeEntities, Entity regionEntity)
        {
            var length = edgeEntities.Length;
            var result = new HashSet<Entity>();

            for (var i = 0; i < length; i++)
            {
                var edge = World.Active.EntityManager.GetComponentData<Edge>(edgeEntities[i]);
                if (edge.A == regionEntity)
                {
                    result.Add(edge.B);
                    continue;
                }

                if (edge.B == regionEntity)
                    result.Add(edge.A);

            }

            return result;
        }

    }
}