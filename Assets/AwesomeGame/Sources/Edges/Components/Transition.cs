using Unity.Entities;

namespace AwesomeGame.Edges.Components
{
    public enum TransitionState
    {
        InTransit,
        ReadyForProcessing,
        Processed
    }

    public struct Transition : IComponentData
    {
        public float Time;
        public Entity Target;
        public Entity Source;
        public TransitionState State;
    }
}