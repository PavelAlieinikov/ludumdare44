using System;
using Unity.Entities;

namespace AwesomeGame.Edges.Components
{
    [Serializable]
    public struct Edge : IComponentData
    {
        public Guid Id;
        public Entity A;
        public Entity B;
    }
}