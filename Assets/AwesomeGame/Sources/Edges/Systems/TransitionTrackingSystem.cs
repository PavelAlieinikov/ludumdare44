using AwesomeGame.Edges.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Edges.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class TransitionTrackingSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Transition>());
        }

        protected override void OnUpdate()
        {
            if(!this.IsPlaying())
                return;

            Entities.With(_query)
                .ForEach((Entity entity, ref Transition transition) =>
                {
                    if (transition.State != TransitionState.InTransit)
                        return;

                    transition.Time -= Time.deltaTime;
                    if (transition.Time > 0)
                        return;

                    if (!EntityManager.HasComponent<Region>(transition.Target))
                        Debug.LogError($"Transition was sent not to region {transition.Target}!");

                    transition.State = TransitionState.ReadyForProcessing;
//                    Debug.Log("Transaction ended");
                });
        }
    }
}