using System;
using AwesomeGame.Edges.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using JetBrains.Annotations;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.Edges.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class EdgeLinkingSystem : ComponentSystem
    {
        private EntityQuery _edgesQuery;
        private EntityQuery _regionsQuery;

        protected override void OnCreateManager()
        {
            _edgesQuery = EntityManager.CreateEntityQuery(ComponentType.ReadWrite<Edge>(),
                                                          ComponentType.ReadOnly<EdgeLinks>());

            _regionsQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>());
        }

        protected override void OnUpdate()
        {
            if (_edgesQuery.CalculateLength() == 0)
                return;

            Entities.With(_edgesQuery)
                .ForEach((Entity entity, EdgeLinks links, ref Edge edge) =>
                {
                    if(edge.A != default(Entity))
                        return;

                    edge.A = GetEntityById(links.RegionA);
                    edge.B = GetEntityById(links.RegionB);

                    PostUpdateCommands.RemoveComponent<EdgeLinks>(entity);

                    Debug.Log($"{edge.A} linked to {edge.B} through {entity}");
                });
        }

        private Entity GetEntityById(Entity regionEntity)
        {
            var result = default(Entity);
            Entities.With(_regionsQuery)
                .ForEach((Entity entity, ref Region region) =>
                {
                    if (entity == regionEntity)
                        result = entity;
                });
            return result;
        }
    }
}