﻿using AwesomeGame.Edges.Components;
using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using Unity.Entities;

namespace AwesomeGame.Edges.Systems
{
    public class TransitionCleanupSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(
                ComponentType.ReadOnly<Transition>());
        }

        protected override void OnUpdate()
        {
            Entities.With(_query).ForEach(
                (Entity entity, ref Transition transition) =>
                {
                    if (transition.State != TransitionState.Processed)
                        return;

                    PostUpdateCommands.DestroyEntity(entity);
                });
        }
    }
}