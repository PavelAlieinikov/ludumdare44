using System;
using Unity.Entities;
using Unity.Mathematics;

namespace AwesomeGame.Edges.Systems
{
    [Serializable]
    public struct EdgePoints : IComponentData
    {
        public float3x2 Value;
    }
}