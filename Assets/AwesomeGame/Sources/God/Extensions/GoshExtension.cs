using AwesomeGame.God.Components;
using Unity.Collections;
using Unity.Entities;

namespace AwesomeGame.God.Extensions
{
    public static class GoshExtension
    {
        public static bool IsPlaying(this ComponentSystem system)
        {
            return system.HasSingleton<Gosh>() && system.GetSingleton<Gosh>().State == SessionState.Playing;
        }

        public static void Cleanup<TType>(this EntityManager entityManager)
        {
            var query = entityManager.CreateEntityQuery(typeof(TType));
            var array = query.ToEntityArray(Allocator.Persistent);
            foreach (var e in array)
                entityManager.DestroyEntity(e);
            array.Dispose();
        }
    }
}