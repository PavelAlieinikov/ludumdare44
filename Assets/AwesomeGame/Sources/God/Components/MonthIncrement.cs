﻿using AwesomeGame.Regions.Components;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.God.Components
{
    public struct MonthIncrement : IComponentData, ICronoComponent
    {
        public float MonthPeriod;

        public float IncrementTime => MonthPeriod;
        public float Timer { get; set; }
    }
}