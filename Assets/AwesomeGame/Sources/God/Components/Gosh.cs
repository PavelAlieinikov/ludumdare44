﻿using System;
using Unity.Entities;

namespace AwesomeGame.God.Components
{
    public enum SessionState
    {
        None,
        Pause,
        Playing,
        Win,
        Loos
    }

    public struct Gosh : IComponentData
    {
        public Guid Guid;

        public int Mana;

        public int Score;

        public SessionState State;
    }
}