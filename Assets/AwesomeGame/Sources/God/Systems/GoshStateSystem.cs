﻿using System;
using AwesomeGame.Armies.Components;
using AwesomeGame.Edges.Components;
using AwesomeGame.God.Components;
using AwesomeGame.Regions.Extensions;
using Unity.Collections;
using Unity.Entities;

namespace Assets.AwesomeGame.Sources.God.Systems
{
    public class GoshStateSystem : ComponentSystem
    {
        protected override void OnStartRunning()
        {
        }

        public void Initialize()
        {
            var entity = GetSingletonEntity<Gosh>();

            EntityManager.SetComponentData(
                entity,
                new Gosh()
                {
                    Guid = Guid.NewGuid(),
                    State = SessionState.Playing,
                    Mana = 100,
                    Score = 0,
                });

            
            EntityManager.SetComponentData(
                entity,
                new MonthIncrement()
                {
                    MonthPeriod = 1f
                });
        }

        protected override void OnStopRunning()
        {
        }

        protected override void OnDestroyManager()
        {
            if (!HasSingleton<Gosh>())
                return;

            var entity = GetSingletonEntity<Gosh>();
            EntityManager.DestroyEntity(entity);


        }



        protected override void OnUpdate()
        {
            if (!HasSingleton<Gosh>())
                return;

            Entities.ForEach(
                (ref Gosh gosh, ref MonthIncrement monthIncrement) =>
                {
                    if (!ChronoUtils.IsTicked(ref monthIncrement))
                        return;

                    gosh.Score++;
                });
        }
    }
}