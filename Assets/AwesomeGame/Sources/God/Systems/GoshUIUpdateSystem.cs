﻿using AwesomeGame.God.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.UI;
using JetBrains.Annotations;
using Slash.Unity.DataBind.Core.Presentation;
using Unity.Entities;
using UnityEngine;

namespace AwesomeGame.God.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class GoshUIUpdateSystem : ComponentSystem
    {
        private UIManager UIManager;

        protected override void OnStartRunning()
        {
            var hud = GameObject.FindGameObjectWithTag("GameUI").transform;
            UIManager = (UIManager)hud.GetComponentInChildren<ContextHolder>().Context;
        }

        protected override void OnUpdate()
        {
            if (!this.HasSingleton<Gosh>())
                return;
           
            if (GetSingleton<Gosh>().State == SessionState.Loos )
            {
                UIManager.LooseScreenVisible = true;
                return;
            }

            if (GetSingleton<Gosh>().State == SessionState.Win)
            {
                UIManager.WinScreenVisible = true;
                return;
            }

            if(GetSingleton<Gosh>().State != SessionState.Playing)
                return;

            var gosh = GetSingletonEntity<Gosh>();
            var data = EntityManager.GetComponentData<Gosh>(gosh);
            UIManager.Mana = data.Mana;
            UIManager.Score = data.Score;
        }
    }
}