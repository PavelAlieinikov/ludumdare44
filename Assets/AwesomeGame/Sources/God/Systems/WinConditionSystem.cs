using AwesomeGame.God.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Systems;
using JetBrains.Annotations;
using Unity.Entities;

namespace AwesomeGame.God.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    [UpdateAfter(typeof(InitRegions))]
    public class WinConditionSystem : ComponentSystem
    {
        private EntityQuery _regionsQuery;
        private EntityQuery _goshHavingsQuery;

        protected override void OnCreateManager()
        {
            _regionsQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>());
            _goshHavingsQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Region>(),
                                                            ComponentType.ReadOnly<GoshHavings>());
        }

        protected override void OnUpdate()
        {
            var count = _regionsQuery.CalculateLength();
            if(!this.IsPlaying() || count == 0)
                return;

            var gosh = GetSingleton<Gosh>();
            var goshHavings = _goshHavingsQuery.CalculateLength();
            if (count == goshHavings)
            {
                gosh.State = SessionState.Win;
                SetSingleton(gosh);
            }
            else if(gosh.Mana <= 0)
            {
                gosh.State = SessionState.Loos;
                SetSingleton(gosh);
            }

            if (goshHavings > 0)
                return;

            gosh.Mana -= 1;
            if (gosh.Mana < 0)
                gosh.Mana = 0;
            SetSingleton(gosh);
        }
    }
}