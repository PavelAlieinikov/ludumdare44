using System;
using AwesomeGame.Armies.Components;
using AwesomeGame.God.Extensions;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Entities;
using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace AwesomeGame.Armies.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class MigrationInitSystem : ComponentSystem
    {
        private EntityQuery _migrationQuery;

        protected override void OnCreateManager()
        {
            var migrationArchetype = new[]
            {
                ComponentType.ReadOnly<Region>(),
                ComponentType.ReadOnly<AIAgent>(),
                ComponentType.ReadOnly<Population>(),
                ComponentType.Exclude<Migrants>(),
                ComponentType.Exclude<Defunct>()
            };

            _migrationQuery = EntityManager.CreateEntityQuery(migrationArchetype);
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            Entities.With(_migrationQuery)
                .ForEach((Entity entity, ref Population population, ref AIAgent agent) =>
                {
                    if(!agent.IsActive)
                        return;

                    if (!ChronoUtils.IsTicked(ref agent))
                        return;

                    if (!SuccessDiceThrow(agent.Chance))
                        return;

                    var migrants = GetMigrants(agent, population);
                    if(migrants.Heretics + migrants.Believers <= 0)
                        return;

                    var total = population.Total();
                    var isEnemy = EntityManager.GetOwnerOf(entity) == Guid.Empty;
                    if(isEnemy && total - migrants.Heretics < agent.MinPopulation)
                        return;
                    if(!isEnemy &&  total - migrants.Believers < agent.MinPopulation)
                        return;

                    PostUpdateCommands.AddComponent(entity, migrants);
//                    var isEnemy = EntityManager.GetOwnerOf(entity) == Guid.Empty;
//                    Debug.Log($"Enemy {isEnemy} Believers {migrants.Believers}, Heretics = {migrants.Heretics} want to migrate");
                });
        }

        private static Migrants GetMigrants(AIAgent agent, Population population)
        {
            var ration = math.clamp(agent.MigrationRation, 0, 1);
            var migrants = new Migrants
            {
                Heretics = (int) math.ceil(population.Heretics * ration),
                Believers = (int) math.ceil(population.Believers * ration)
            };
            return migrants;
        }

        private static bool SuccessDiceThrow(float chance) => Random.value <= chance;
    }
}