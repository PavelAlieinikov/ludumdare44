using AwesomeGame.Armies.Components;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace AwesomeGame.Armies.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class ArmyInitSystem : ComponentSystem
    {
        private IconsPool _iconsPool;
        private EntityQuery _query;

        protected override void OnStartRunning()
        {
            _iconsPool = Object.FindObjectOfType<IconsPool>();
        }

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<ArmyView>(),
                                                     ComponentType.ReadOnly<Translation>(),
                                                     ComponentType.Exclude<Transform>());
        }

        protected override void OnUpdate()
        {
            if(_query.CalculateLength() == 0)
                return;

            var entities = _query.ToEntityArray(Allocator.Persistent);
            foreach (var entity in entities)
            {
                var army = EntityManager.GetComponentData<ArmyView>(entity);
                var transform = _iconsPool.Create(army.Type);
                EntityManager.AddComponentObject(entity, transform);
                transform.position = EntityManager.GetComponentData<Translation>(entity).Value;
                //TODO: Get time, get target position
            }
            entities.Dispose();
        }
    }
}