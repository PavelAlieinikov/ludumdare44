using System;
using System.Collections.Generic;
using System.Linq;
using AwesomeGame.Armies.Components;
using AwesomeGame.Edges.Components;
using AwesomeGame.Edges.Extensions;
using AwesomeGame.God.Extensions;
using AwesomeGame.Interactions.Components;
using AwesomeGame.Regions.Components;
using AwesomeGame.Regions.Extensions;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AwesomeGame.Armies.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class MigrationSystem : ComponentSystem
    {
        private EntityQuery _migrationQuery;
        private EntityQuery _edgesQuery;

        protected override void OnCreateManager()
        {
            var migrationArchetype = new[]
            {
                ComponentType.ReadOnly<Region>(),
                ComponentType.ReadOnly<AIAgent>(),
                ComponentType.ReadOnly<Migrants>()
            };

            _migrationQuery = EntityManager.CreateEntityQuery(migrationArchetype);
            _edgesQuery = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<Edge>());
        }

        protected override void OnUpdate()
        {
            if (!this.IsPlaying())
                return;

            if (_migrationQuery.CalculateLength() == 0)
                return;

            var entities = _migrationQuery.ToEntityArray(Allocator.Persistent);
            var edgeEntities = _edgesQuery.ToEntityArray(Allocator.Persistent);

            foreach (var entity in entities)
            {
                var agent = EntityManager.GetComponentData<AIAgent>(entity);

                var neighbors = edgeEntities.GetNeighborsOf(entity).ToList();
                var migrationTarget = SelectMigrationTarget(entity, agent, neighbors);
                if (migrationTarget == Entity.Null)
                {
                    EntityManager.RemoveComponent<Migrants>(entity);
                    continue;
                }

                bool believers;
                var migrationEdge = edgeEntities.FindEdge(entity, migrationTarget);
                var migrants = GetMigrants(entity, migrationTarget, out believers);

                ReduceSourcePopulation(entity, believers, migrants);
                var isWar = EntityManager.GetOwnerOf(entity) != EntityManager.GetOwnerOf(migrationTarget);
                var finish = ExecuteMigrationView(entity, migrationEdge, isWar, agent.MigrationTime);
                ExecuteTransition(entity, migrationTarget, migrants, believers, finish, agent.MigrationTime);

                EntityManager.RemoveComponent<Migrants>(entity);
            }

            edgeEntities.Dispose();
            entities.Dispose();
        }

        private Entity SelectMigrationTarget(Entity sourceEntity, AIAgent agent, List<Entity> possibleTargets)
        {
            if (possibleTargets.Count == 0)
                return Entity.Null;

            possibleTargets = WithoutInactiveRegions(possibleTargets);
            if (EntityManager.GetOwnerOf(sourceEntity) != Guid.Empty)
            {
                var faith = EntityManager.GetComponentData<Faith>(sourceEntity);
                if (faith.Value < agent.FaithRequire)
                    possibleTargets = WithoutEnemyRegions(possibleTargets);
            }

            if (possibleTargets.Count == 0)
                return Entity.Null;

            var index = Random.Range(0, possibleTargets.Count);
            if (index >= possibleTargets.Count)
                index = possibleTargets.Count - 1;
            return possibleTargets[index];
        }

        private List<Entity> WithoutInactiveRegions(List<Entity> possibleTargets)
        {
            var availableTarget = new List<Entity>(possibleTargets.Count);
            foreach (var entity in possibleTargets)
            {
                if(EntityManager.GetComponentData<AIAgent>(entity).IsActive)
                    availableTarget.Add(entity);
            }

            return availableTarget;
        }

        private List<Entity> WithoutEnemyRegions(List<Entity> possibleTargets)
        {
            var availableTarget = new List<Entity>(possibleTargets.Count);
            foreach (var entity in possibleTargets)
            {
                if (EntityManager.GetOwnerOf(entity) != Guid.Empty)
                    availableTarget.Add(entity);
            }

            return availableTarget;
        }

        private int GetMigrants(Entity sourceEntity, Entity targetEntity, out bool believers)
        {
            var sourceOwner = EntityManager.GetOwnerOf(sourceEntity);
            var targetOwner = EntityManager.GetOwnerOf(targetEntity);
            var migrants = EntityManager.GetComponentData<Migrants>(sourceEntity);

            believers = false;
            //Enemy migration
            if (sourceOwner == Guid.Empty)
                return migrants.Heretics;

            believers = true;
            //Attack Enemy migration
            if (sourceOwner != targetOwner)
                return migrants.Believers;

            //Random migration
            believers = Random.value > 0.3 || migrants.Heretics == 0;
            return believers ? migrants.Believers : migrants.Heretics;
        }

        private void ExecuteTransition(Entity sourceEntity, Entity targetEntity, int migrants, bool believers,
            float3 finish, float time)
        {
            var transition = new Transition
            {
                Time = time,
                Target = targetEntity,
                State = TransitionState.InTransit,
                Source = sourceEntity
            };

            var attack = new Attack
            {
                Owner = EntityManager.GetOwnerOf(sourceEntity),
                Believers = believers,
                Migrants = migrants,
                Source = sourceEntity,
                TargetPosition = finish
            };

            var migrationEntity = EntityManager.CreateEntity();
            EntityManager.AddComponentData(migrationEntity, transition);
            EntityManager.AddComponentData(migrationEntity, attack);
//            Debug.Log($"{migrants} migrants from {sourceEntity} migrates to {targetEntity}. {time}s");
        }

        private float3 ExecuteMigrationView(Entity sourceEntity, Entity edgeEntity, bool war, float time)
        {
            var points = edgeEntity.GetEdgePoints(sourceEntity);
            return ExecuteMigrationView(war ? IconType.Army : IconType.Migrants,
                                        points.c0, points.c1,  time);
        }

        private float3 ExecuteMigrationView(IconType type, float3 start, float3 finish, float time)
        {
            var view = EntityManager.CreateEntity();
            if (time <= 0)
                time = Time.deltaTime * 10;
            var speed = math.distance(start, finish) / time;
            EntityManager.AddComponentData(view, new Translation {Value = start});
            EntityManager.AddComponentData(view, new ArmyView
            {
                Target = finish,
                Speed = speed,
                Type = type
            });
            return finish;
        }

        private void ReduceSourcePopulation(Entity entity, bool believers, int migrants)
        {
            var population = EntityManager.GetComponentData<Population>(entity);
            if (believers)
                population.Believers -= migrants;
            else
                population.Heretics -= migrants;

            EntityManager.SetComponentData(entity, population);
        }
    }
}