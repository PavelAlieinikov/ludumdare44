using AwesomeGame.Armies.Components;
using AwesomeGame.God.Extensions;
using JetBrains.Annotations;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace AwesomeGame.Armies.Systems
{
    [UsedImplicitly]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ArmyMovementSystem : ComponentSystem
    {
        private EntityQuery _query;

        protected override void OnCreateManager()
        {
            _query = EntityManager.CreateEntityQuery(ComponentType.ReadOnly<ArmyView>(),
                                                     ComponentType.ReadOnly<Translation>(),
                                                     ComponentType.ReadOnly<Transform>());
        }

        protected override void OnUpdate()
        {
            if(!this.IsPlaying())
                return;

            Entities.With(_query)
                .ForEach((Entity entity, Transform transform, ref Translation translation,
                             ref ArmyView armyView) =>
                         {
                             var vector = math.normalize(armyView.Target - translation.Value);
                             translation.Value = translation.Value + vector * armyView.Speed * Time.deltaTime;
                             transform.position = translation.Value;

                             if(math.distance(translation.Value, armyView.Target) > 0.1)
                                 return;

                             PostUpdateCommands.DestroyEntity(entity);
                             Object.Destroy(transform.gameObject);
                         });
        }
    }
}