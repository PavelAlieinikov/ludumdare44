using Unity.Entities;

namespace AwesomeGame.Armies.Components
{
    public struct Migrants : IComponentData
    {
        public int Believers;
        public int Heretics;
    }
}