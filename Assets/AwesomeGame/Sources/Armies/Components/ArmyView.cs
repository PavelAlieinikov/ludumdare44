using Unity.Entities;
using Unity.Mathematics;

namespace AwesomeGame.Armies.Components
{
    public struct ArmyView : IComponentData
    {
        public IconType Type;
        public float3 Target;
        public float Speed;
    }
}