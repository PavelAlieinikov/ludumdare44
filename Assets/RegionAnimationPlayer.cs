﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionAnimationPlayer : MonoBehaviour
{
    [SerializeField]
    public Animator Bless;

    [SerializeField]
    public Animator Curse;

    [SerializeField]
    public Animator Blood;

    public void PlayBless()
    {
        Bless.SetTrigger("Bless");
    }

    public void PlayCurse()
    {
        Curse.SetTrigger("Curse");
    }

    public void PlayBlood()
    {
        Blood.SetTrigger("Blood");
    }
}
